import { Component } from '@angular/core';
import {AppService} from "./app.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular';

  data:any = "";

  constructor(private appService: AppService) { }

  ngOnInit() {

    this.appService.sendGetRequest()
      .subscribe(
        data => {
          this.data = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });

  }

}

