# Fronted docker image

## How to develop
You have to install npm (Node js).

If you want new app, you can remove directory `angular` and `ng new angular`   

## How to run:
Execute command `docker-compose up` with [docker-compose-file](https://gitlab.com/larry-brand-devops-aws-infra-example/ci-cd/kubernetes/-/blob/master/local/docker-compose.yml)

or execute command

`$docker run --name example-frontend -itd -p 80:80 larrybrand/devops-aws-infra-example-frontend`

## How to run in k8s cluster:
Open port 80 and pass environment variables "ENVIRONMENT=prod"
