FROM node:14 AS build

COPY angular /tmp/angular

WORKDIR /tmp/angular

RUN npm install && npm run build

FROM nginx:alpine
COPY --from=build /tmp/angular/dist/angular /usr/share/nginx/html
#COPY nginx/nginx.conf /etc/nginx/nginx.conf
#RUN chown nginx:nginx /var/cache/nginx && chmod 770 /var/cache/nginx && \
#    mkdir -p /var/nginx/ && \
#    touch /var/nginx/nginx.pid && chown nginx:nginx /var/nginx/nginx.pid && \
#    chmod 770 /var/nginx/nginx.pid
#USER nginx
EXPOSE 80